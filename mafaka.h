#ifndef MAFAKA_H
#define MAFAKA_H

#include "quantum.h"

// This a shortcut to help you visually see your layout.
// The following is an example using the Planck MIT layout
// The first section contains all of the arguements
// The second converts the arguments into a two-dimensional array
#define KEYMAP( \
    k00, k01,           k02, k03,\
    k10, k11, k12, k13, k14, k15,\
    k20, k21,      k22, k23, k24\
) \
{ \
    { k00, k01, KC_NO, KC_NO, k02, k03 }, \
    { k10, k11, k12,   k13,   k14, k15 }, \
    { k20, k21, KC_NO, k22,   k23, k24 }, \
}

#endif
