#include "mafaka.h"

#define _DEFAULT 0
#define _FUNCTION 1
#define _____ KC_TRNS
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  /* DEFAULT
  * ,--------------------------------------------------------------------------------------------------------------.
  * | Home Score 1  |   Visitor Score 1  |||||||||||||||||||||||||||||||||| Start/Stop Clock |                     |
  * |---------------+--------------------+--------------+-----------------+------------------+---------------------|
  * | Home Score 2  |   Visitor Score 2  | Home Foul 1  | Visitor Foul 1  |                  |         Buzzer      |
  * |---------------+--------------------+--------------+-----------------+------------------+---------------------|
  * | Home Score 3  |   Visitor Score 3  |    FUNCTION  |                 | Reset Shot Clock |  Reset Shot Clock 2 |
  * `--------------------------------------------------------------------------------------------------------------'
  */
  [_DEFAULT] = {
    {KC_1, KC_2, KC_NO,         KC_NO, KC_ENT,  KC_NO},
    {KC_Q, KC_W, KC_Z,          KC_X,  KC_NO,   KC_NO},
    {KC_A, KC_S, MO(_FUNCTION), KC_NO, KC_BSPC, KC_BSLS}
  },

  /* FUNCTION
  * ,----------------------------------------------------------------------------------------.
  * |  HOME TIMEOUT  |   VISITOR TIMEOUT   ||||||||||||||||||||||||||||||||||||    |  RESET  |
  * |----------------+---------------------+--------------+-------------------+----+---------|
  * |                |                     | Home Foul -1  | Visitor Foul -1  |    |         |
  * |----------------+---------------------+---------------+------------------+----+---------|
  * | Home Score -1  |   Visitor Score -1  |               |                  |    |         |
  * `----------------------------------------------------------------------------------------'
  */
  [_FUNCTION] = {
    {KC_T,  KC_Y,  KC_NO,  KC_NO, _____, _____},
    {_____, _____, KC_D,   KC_F,  _____, _____},
    {KC_E,  KC_R,  _____,  _____, _____, _____}
  },
};

const uint16_t PROGMEM fn_actions[] = {};

// void persistant_default_layer_set(uint16_t default_layer) {
//   eeconfig_update_default_layer(default_layer);
//   default_layer_set(default_layer);
// }

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
  // MACRODOWN only works in this function
      switch(id) {
        case 0:
          if (record->event.pressed) {
            register_code(KC_RSFT);
          } else {
            unregister_code(KC_RSFT);
          }
        break;
      }
    return MACRO_NONE;
};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

// bool process_record_user(uint16_t keycode, keyrecord_t *record) {
//   switch (keycode) {
//     case DELUXE:
//       if (record->event.pressed) {
//         persistant_default_layer_set(1UL<<_DELUXE);
//       }
//       return false;
//       break;
//     case PRO:
//       if (record->event.pressed) {
//         persistant_default_layer_set(1UL<<_DELUXE);
//       }
//       return false;
//       break;
//   }
//   return true;
// }

void led_set_user(uint8_t usb_led) {

}